import re
import sys


def parse_file(filename):
    s = open(filename).read().split("\n")
    pins = {}
    pinscount = 0
    # ctitle=re.compile(r'Device/Package\s*(\S*)\s*(\S*)\s*(\S*)\s*')
    ccomment = re.compile(r"^--[\s\S]*")
    chead = re.compile(
        r"Pin\s*Pin Name\s*Memory Byte Group\s*Bank\s*VCCAUX Group\s*Super Logic Region\s*I/O Type\s*No-Connect\s*"
    )
    chead = re.compile(r"Pin Name|Pin|Memory Byte Group|Bank|VCCAUX Group|Super Logic Region|I/O Type|No-Connect")
    #            headslist=re.findall(headsstr,l)
    # heads="Pin Name|Pin|Memory Byte Group|Bank|VCCAUX Group|Super Logic Region|I/O Type|No-Connect"
    # h1=re.findall(titles,t1)
    # h2=re.findall(titles,t2)
    cpin = re.compile(r"(\S+\d+)\s+(\S*)\s+(\S+)\s+(\S+)\s+(\S*)\s+(\S+)\s+(\S+)\s+(\S+)\s*")
    ctail = re.compile(r"Total Number of Pins Generated,\s*(\d+)")
    for l in s:
        mcomment = ccomment.match(l)
        # mtitle=ctitle.match(l)
        mhead = chead.findall(l)
        mpin = cpin.match(l)
        mtail = ctail.match(l)
        if mcomment:
            # print('comment',l)
            pass
        # elif mtitle:
        #     package=mtitle.group(1)
        #     date=mtitle.group(2)
        #     time=mtitle.group(3)
        elif mhead:
            spin = r"\s+".join(
                [r"(?P<%s>\S+)" % (h.replace(" ", "_").replace("/", "_").replace("-", "_")) for h in mhead]
            )
            cpin = re.compile(spin)
            # print('spin',spin)
        elif mpin:
            #    pins[mpin.group(1)]={'package_pin':mpin.group(1),'pinname':mpin.group(2),'memory_byte_group':mpin.group(3),'bank':mpin.group(4),'vccaux_group':mpin.group(5),'super_logic_region':mpin.group(6),'iotype':mpin.group(7),'no_connect':mpin.group(8)}
            # print(l,mpin)
            pins[mpin.group(1)] = mpin.groupdict()
            # print('mpin',mpin.groupdict())
            pinscount = pinscount + 1
        elif mtail:
            if pinscount != eval(mtail.group(1)):
                print("warning: pin number %d doesn't match specified in file %d" % (pinscount, eval(mtail.group(1))))
        else:
            # print('not match',l)
            pass
    #    if iostandardfile:
    #        with open(iostandardfile) as fio:
    #            iostandard=json.load(fio)
    #            for pin in pins:
    #                if (pins[pin]['iotype'] in ['HR','HP']):
    #                    pins[pin]['iostandard']=iostandard[pins[pin]['iotype']]
    return pins


if __name__ == "__main__":
    import pprint

    pprint.pprint(parse_file(sys.argv[1]))
#    fpga=xilinx_pinmap(sys.argv[1])
#    fpga.verilog_gen(fpga.verilog_io_pin(),'t2.v','t2','')
#    fpga.xdcs_gen(fpga.verilog_io_pin(),'t2.xdc')
#    [xdc,ucf,verilog]=fpga.verilog_top(fpga.verilog_io_pin())
#    print xdc
#    print verilog
