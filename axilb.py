import json
import numpy

# "fclk100":{"access": "r", "addr_width": 0, "data_width": 32, "sign": "unsigned", "init": "0", "base_addr": 1}


class addrspace:
    def __init__(self):
        self.addr = numpy.empty((0, 2), dtype=int)

    def add(self, base, width):
        self.addr = numpy.append(self.addr, numpy.array([[base, base + (1 << width) - 1]]), axis=0)
        self.sort()

    def append(self, otheraddrspace):
        self.addr = numpy.append(self.addr, otheraddrspace.addr, axis=0)
        self.sort()

    def sort(self):
        # saddr=sorted(self.addr,key=lambda i:i[0])
        self.addr.sort(axis=0)

    def conflict(self, base, width):
        up = base + (1 << width) - 1
        a1d0 = self.addr[:, 0]
        a1d1 = self.addr[:, 1]
        i1 = numpy.searchsorted(a1d0, base)
        i2 = numpy.searchsorted(a1d1, base)
        i3 = numpy.searchsorted(a1d0, up)
        i4 = numpy.searchsorted(a1d1, up)
        if i1 == i2 and i1 == i3 and i1 == i4:
            if len(self.addr) == 0:
                conflict = False
            else:
                if i1 == len(self.addr):
                    conflict = False
                else:
                    conflict = self.addr[i1, 0] in [up, base] or self.addr[i1, 1] in [up, base]
        else:
            conflict = True
        return conflict

    def diff(self, width=None):
        if width == 0 or width is None:
            addr = self.addr
        else:
            addr = self.addr >> width
        return numpy.array([addr[0][0] - 0 if n == 0 else addr[n][0] - addr[n - 1][1] for n in range(addr.shape[0])])

    def nextbase(self, width, align=True):
        if self.addr.shape[0] == 0:
            baseaddr = 0
        else:
            if align:
                diffused = self.diff(width=width)
                firstgt2 = numpy.where(diffused > 1)[0]
                if len(firstgt2) == 0:
                    firstgt2 = len(self.addr) - 1
                else:
                    firstgt2 = firstgt2[0] - 1

                firstavail = (((self.addr[firstgt2][1]) >> width) + 1) if firstgt2 >= 0 else 0
                baseaddr = firstavail << width
            else:
                diffused = self.diff(width=0)
                firstgt2 = numpy.where(diffused > (1 << width))[0]
                if len(firstgt2) == 0:
                    firstgt2 = len(self.addr) - 1
                else:
                    firstgt2 = firstgt2[0] - 1
                firstavail = self.addr[firstgt2][1] + 1
                baseaddr = firstavail
        self.add(baseaddr, width)
        return int(baseaddr)

    def valid(self):
        return all(self.diff() > 0)

    def lastaddr(self):
        return self.addr[-1][-1]


class regs:
    def __init__(self, jsondict, busdict, avoidjson=[], keep=True):
        self.regs = []
        self.busdict = busdict
        self.addrspace = addrspace()
        for jdictfile in avoidjson:
            with open(jdictfile) as avoidregmapfile:
                avoidjdict = json.load(avoidregmapfile)
            rkeep = regs(avoidjdict, busdict=busdict, keep=True)
            print(self.addrspace)
            self.addrspace.append(rkeep.addrspace)
        for k, v in jsondict.items():
            self.regs.append(reg(name=k, propdict=v, busdict=busdict))
        self.assign(keep=keep)
        for r in self.regs:
            r.setbusaddrwidth(self.minbusaddrwidth())
        self.sorted()

    def assign(self, keep=True):
        self.sorted()
        if keep:
            # self.assigned=[(r.base_addr,r.base_addr+(1<<r.addr_width)-1) for r in self.regs if r.base_addr!=-1]
            for r in self.regs:
                if r.base_addr != -1:
                    if not self.addrspace.conflict(r.base_addr, r.addr_width):
                        self.addrspace.add(r.base_addr, r.addr_width)
                    else:
                        print("conflicted address, can not keep", r.name, r.base_addr)
                        r.base_addr = -1
            unassigned = [r for r in self.regs if r.base_addr == -1]
        else:
            # self.assigned=[]
            unassigned = self.regs
        #        unusedaddr=[i for i in range(1,len(self.regs)+1) if i not in self.assigned]
        for index, r in enumerate(unassigned):
            # r.base_addr=unusedaddr[index]
            if r.addr_width > 1:
                pass
            r.base_addr = self.addrspace.nextbase(r.addr_width)
        self.sorted()

    def sorted(self):
        # sregs=sorted(self.regs,key=lambda i:'%8d%8d'%(i.addr_width,i.base_addr))
        sregs = sorted(self.regs, key=lambda i: "%8d%8d" % (i.base_addr, i.addr_width))
        self.regs = sregs

    def modportin(self):
        return ",".join([j for j in [i.modportin() for i in self.regs] if j])

    def modportout(self):
        return ",".join([j for j in [i.modportout() for i in self.regs] if j])

    def ro(self):
        return ",".join([i.name for i in self.regs if i.raccess])

    def rw(self):
        return ",".join([i.name for i in self.regs if i.waccess])

    #    def modport(self):
    #        print(self.rw())
    #        return ','.join(['%s,stb_%s'%(i,i) for i in [i.name for i in self.regs if i.waccess]])

    def definestr(self):
        return "\n".join([i.definestr() for i in self.regs])

    def writestr(self):
        return "\n".join([i.writestr() for i in self.regs if i.waccess])

    def readstr(self):
        return "\n".join([i.readstr() for i in self.regs if i.raccess])

    def readramstr(self):
        return "\n".join([j for j in [i.readramstr() for i in self.regs if i.raccess] if j is not None])

    def jsondict(self):
        return {r.name: r.propdict for r in self.regs}

    def infodict(self):
        dinfo = {k: v for k, v in self.busdict.items()}
        dinfo.update(
            localbusname=clargs.outname,
            rwregs=self.rw(),
            roregs=self.modportout(),
            modport=self.modportin(),
            modportin=self.modportin(),
            modportout=self.modportout(),
            lbdefine=self.definestr(),
            lbwrite=self.writestr() + "\n" + self.readramstr(),
            lbread=self.readstr(),
            busaddrwidth=self.minbusaddrwidth(),
        )
        return dinfo

    def minbusaddrwidth(self):
        return int(numpy.ceil(numpy.log2(self.addrspace.lastaddr())))

    def svstr(self):
        return self.busdict["template"] % self.infodict()

    def inputvh(self):
        return "input lbclk,%(modportin)s\n" % self.infodict()

    def updatejson(self, filename):
        self.sorted()
        #        rstrlist=[]
        #        for i,r in enumerate(self.regs):
        #            print(i)
        #            rstrlist.append(r.updatejson())
        with open(filename, "w") as regmapfile:
            regmapfile.write("{\n%s\n}" % ("\n,".join([r.updatejson() for r in self.regs])))


class reg:
    def __init__(self, name, propdict, busdict):
        self.name = name
        self.propdict = {
            "access": "rw",
            "addr_width": 0,
            "data_width": 32,
            "sign": "unsigned",
            "init": "0",
            "base_addr": None,
        }
        self.propdict.update({k: v for k, v in propdict.items() if k in self.propdict})
        self.busdict = busdict
        self.busaddrwidth = busdict["busaddrwidth"]
        if self.propdict["addr_width"] > 0:
            print(name, self.propdict["addr_width"])

    @property
    def addr_width(self):
        return int(eval(str(self.propdict["addr_width"])))

    @property
    def initval(self):
        return int(eval(str(self.propdict["init"])))

    @property
    def sign(self):
        return self.propdict["sign"] == "signed"

    @property
    def base_addr(self):
        base_addr = self.propdict["base_addr"]
        if base_addr is None:
            base_addr = -1
        return base_addr

    @base_addr.setter
    def base_addr(self, value):
        self.propdict["base_addr"] = value

    @property
    def waccess(self):
        return "w" in self.propdict["access"]

    @property
    def raccess(self):
        return "r" in self.propdict["access"]

    def setbusaddrwidth(self, value):
        self.busaddrwidth = value

    def infodict(self):
        infodict = {k: v for k, v in self.propdict.items()}
        infodict.update(self.busdict)
        infodict.update(busaddrwidth=self.busaddrwidth)
        infodict.update(name=self.name)
        infodict.update(base_addr=self.base_addr)
        infodict.update(base_addr_sel=self.base_addr >> self.addr_width)
        infodict.update(
            base_addr_selstr="{%d'd%d,{%d{1'b?}}}"
            % (self.busaddrwidth - self.addr_width, self.base_addr >> self.addr_width, self.addr_width)
        )
        infodict.update(sign="signed " if self.sign else "")
        infodict.update(initval=self.initval)

        return infodict

    def definestr(self):
        retstr = ""
        if self.addr_width == 0:
            retstr = """(* dont_touch="yes" *) wire %(sign)s[%(data_width)d-1:0] %(name)s;\n""" % (self.infodict())
            if self.waccess:
                retstr += (
                    """reg %(sign)s[%(data_width)d-1:0] reg_%(name)s=%(initval)d;reg wgate_%(name)s=0;reg wstb_%(name)s=0;assign %(name)s=reg_%(name)s;"""
                    % (self.infodict())
                )
            if self.raccess:
                retstr += """reg rstb_%(name)s=0;""" % (self.infodict())
        else:
            if self.waccess:
                retstr += (
                    """reg [%(addr_width)d-1:0] waddr_%(name)s=0;reg %(sign)s[%(data_width)d-1:0] wdata_%(name)s=0;reg wen_%(name)s=0;reg wstb_%(name)s=0;reg wgate_%(name)s=0;"""
                    % (self.infodict())
                )

            if self.raccess:
                retstr += (
                    """(*dont_touch="yes"*) reg [%(addr_width)d-1:0] raddr_%(name)s=0;\n(* dont_touch="yes" *) wire %(sign)s[%(data_width)d-1:0] rdata_%(name)s\n;reg ren_%(name)s=0;reg rstb_%(name)s=0;"""
                    % (self.infodict())
                )

        return retstr

    def writestr(self):
        if self.addr_width == 0:
            wstr = (
                """wgate_%(name)s<=wstb_%(name)s;wstb_%(name)s<=(%(buswaddrname)s==%(base_addr)d)&%(buswrenname)s;if (wstb_%(name)s) reg_%(name)s<=wdata[%(data_width)d-1:0];"""
                % self.infodict()
            )
        else:
            wstr = (
                """wgate_%(name)s<=wstb_%(name)s;wstb_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)&%(buswrenname)s;if (wstb_%(name)s) begin wdata_%(name)s<=wdata[%(data_width)d-1:0]; waddr_%(name)s<= waddr[%(addr_width)d-1:0]; end wen_%(name)s<=wstb_%(name)s;// 0x%(base_addr)x"""
                % self.infodict()
            )
        return wstr

    def readramstr(self):
        if self.addr_width == 0:
            rstr = """rstb_%(name)s<=(%(buswaddrname)s==%(base_addr)d)&%(busrdenname)s;""" % self.infodict()
        else:
            # rstr='''rstb_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d])==%(base_addr_sel)d&~%(buswrenname)s;if (1) begin rdata_%(name)s_r<=rdata_%(name)s; raddr_%(name)s<= waddr[%(addr_width)d-1:0]; end ren_%(name)s<=rstb_%(name)s;// 0x%(base_addr)x'''%self.infodict()
            rstr = (
                """rstb_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)&%(busrdenname)s;if (rstb_%(name)s) begin raddr_%(name)s<= waddr[%(addr_width)d-1:0]; end ren_%(name)s<=rstb_%(name)s;// 0x%(base_addr)x"""
                % self.infodict()
            )
            # rstr='''rstb_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d])==%(base_addr_sel)d&~%(buswrenname)s;if (rstb_%(name)s) begin raddr_%(name)s<= waddr[%(addr_width)d-1:0]; end ren_%(name)s<=rstb_%(name)s;// 0x%(base_addr)x'''%self.infodict()
        return rstr

    def readstr(self):
        if self.addr_width == 0:
            rstr = (
                """%(base_addr)d: begin rdata <= %(name)s; rvalid<=%(busrden16name)s[READDELAY]; rvalidlast<=%(busrdenlastname)s[READDELAY]; end"""
                % self.infodict()
            )
        else:
            rstr = (
                """%(base_addr_selstr)s: begin rdata <= rdata_%(name)s;rvalid<=%(busrden16name)s[READDELAY]; rvalidlast<=%(busrdenlastname)s[READDELAY]; end"""
                % self.infodict()
            )
        return rstr

    # w:  wstb, name input
    # rw: wstb,rstb,name  input   if name is written from lb, it should have it there
    # r:  rstb, input name output if name is not written from lb, we have to provide it
    def modportin(self):
        retlist = []
        if self.addr_width == 0:
            if self.waccess:
                retlist.extend([i % self.infodict() for i in ["%(name)s", "wgate_%(name)s", "wstb_%(name)s"]])
            if self.raccess:
                retlist.extend(["rstb_%(name)s" % self.infodict()])
        else:
            if self.waccess:
                retlist.extend(
                    [
                        i % self.infodict()
                        for i in [
                            "waddr_%(name)s",
                            "wdata_%(name)s",
                            "wen_%(name)s",
                            "wgate_%(name)s",
                            "wstb_%(name)s",
                        ]
                    ]
                )
            if self.raccess:
                retlist.extend([i % self.infodict() for i in ["raddr_%(name)s", "ren_%(name)s", "rstb_%(name)s"]])
        return ",".join(retlist)

    def modportout(self):
        #        print('modportout',self.name,self.addr_width)
        retlist = []
        if self.addr_width == 0:
            if self.waccess:
                pass
            elif self.raccess:
                retlist.extend(["%(name)s" % self.infodict()])
        else:
            if self.waccess:
                pass
            if self.raccess:
                retlist.extend(["rdata_%(name)s" % (self.infodict())])
        # print('modportout',retlist)
        return ",".join(retlist)

    def updatejson(self):
        d0 = dict(jsondict=json.dumps(self.propdict, indent=2))
        d0.update(self.infodict())
        return """"%(name)s":%(jsondict)s""" % (d0)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", help="regmap.json file", type=str, dest="regmapfile", required=True)
    parser.add_argument("-b", help="localbus.json file", type=str, dest="localbusfile", required=True)
    parser.add_argument(
        "-o", help="regmap generate module and file name", type=str, dest="outname", default=None, required=True
    )
    parser.add_argument(
        "-u",
        "--updatejson",
        dest="updatejson",
        action="store_true",
        default=False,
        help="update the json file with new allocated address",
    )
    parser.add_argument(
        "-k", "--keepaddr", dest="keep", action="store_true", default=False, help="keep existing address"
    )
    parser.add_argument(
        "--avoid", help="other regs to avoid addr conflict", type=str, dest="avoidjson", nargs="*", default=[]
    )
    clargs = parser.parse_args()
    print("avoid", clargs.avoidjson)
    # filename='regmap.json'
    with open(clargs.regmapfile) as regmapfile:
        print(clargs.regmapfile)
        regmapdict = json.load(regmapfile)
    with open(clargs.localbusfile) as localbusfile:
        localbusdict = json.load(localbusfile)
    regmap = regs(regmapdict, busdict=localbusdict, keep=clargs.keep, avoidjson=clargs.avoidjson)
    #    print(regmap.svstr())
    header = "// machine-generated by axilb.py\n"
    f = open(clargs.outname + ".sv", "w")
    f.write(header + regmap.svstr())
    f.close()
    f = open(clargs.outname + "_input.vh", "w")
    f.write(header + regmap.inputvh())
    f.close()
    if clargs.updatejson:
        print("Updating %s file with new allocated address...." % clargs.regmapfile)
        regmap.updatejson(clargs.regmapfile)
    print(regmap.addrspace.addr)

#    print(regmap.ro())
#    print(regmap.definestr())
#    print(regmap.writestr())
#    print(regmap.readstr())
#    t=reg(name="fclk100",propdict={"access": "r", "addr_width": 0, "data_width": 32, "sign": "unsigned", "init": "0", "base_addr": None})
#    print(t.base_addr)
#    t.base_addr=23
#    print(t.base_addr)
