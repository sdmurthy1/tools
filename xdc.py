import re
import sys
import json

comments_re = r"#([\s\S]*?)\n"
packagepin_re = r'set_property\s+PACKAGE_PIN\s+(?P<pin>\S+)\s*\[get_ports\s+"(?P<port>[\S\s]+?)"\s*\]\s*;'
iostandard_re = r'set_property\s+IOSTANDARD\s+(?P<iostandard>\S+)\s*\[get_ports\s+"(?P<port>[\S\s]+?)"\]\s*;'

# re.compile(packagepin_re).findall(re.sub('#[\s\S]*?\n','',s))
if __name__ == "__main__":
    f = open(sys.argv[1])
    sall = f.read()
    f.close()
    pindir = {"input": [], "output": []}
    if len(sys.argv) > 2:
        with open(sys.argv[2], "r") as f:
            pindir = json.load(f)
        if len(sys.argv) > 3:
            with open(sys.argv[3], "r") as f:
                portsel = f.read().split("\n")
    s = re.sub(comments_re, "", sall)
    pindict = {}
    portdict = {}
    for pin, port in re.compile(packagepin_re).findall(s):
        if pin not in pindict:
            pindict[pin] = dict(port=port)
            if pin in pindir["input"]:
                dir = "input"
            elif pin in pindir["output"]:
                dir = "output"
            else:
                dir = "inout"
            pindict[pin].update(dict(dir=dir))
            portdict[port] = pin
        else:
            print("?? %s exist" % pin)
    for iostandard, port in re.compile(iostandard_re).findall(s):
        if port not in portdict:
            print("port %s do not shows in pin yet" % port)
        else:
            pindict[portdict[port]].update(dict(iostandard=iostandard))
    with open(sys.argv[1] + ".json", "w") as f:
        json.dump({p: d for p, d in pindict.items() if d["port"] in portsel}, f, indent=4)

# python xdc.py
