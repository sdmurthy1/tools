def port(name, pindict):
    sl = []
    for p, d in pindict.items():
        sl.append("%s FPGA_%s" % (d["dir"], p))
    return "\n,".join(sl)


def sv(name, wirelist):
    s = """interface %s();
    %s
endinterface"""
    wires = "\n".join(["wire %s;" % p for p in wirelist])
    return s % (name, wires)


def via_sim(name, pindict):
    linout = []
    sinput = []
    soutput = []
    for p, d in pindict.items():
        if d["dir"] == "input":
            sinput.append("assign fpga_%s=fpga.%s;" % (p.lower(), p.lower()))
        elif d["dir"] == "output":
            soutput.append("assign fpga.%s=fpga_%s;" % (p.lower(), p.lower()))
        elif d["dir"] == "inout":
            linout.append(p)
        else:
            print("error direction")

    width = len(linout)
    if width > 0:
        p1 = ",".join(["fpga_%s" % p.lower() for p in linout])
        p2 = ",".join(["fpga.%s" % p.lower() for p in linout])
        svia = """viabus #(.WIDTH(%d))
%svia (.a({%s}),
    .b({%s})
);"""
        sinout = svia % (width, name, p1, p2)

    return "\n".join(["\n".join(sinput), sinout, "\n".join(soutput)])


def via(name, pindict):
    linout = []
    sinput = []
    soutput = []
    for p, d in pindict.items():
        if d["dir"] == "input":
            sinput.append("assign fpga.%s=FPGA_%s;" % (p, p))
        elif d["dir"] == "output":
            soutput.append("assign FPGA_%s=fpga.%s;" % (p, p))
        elif d["dir"] == "inout":
            linout.append(p)
        else:
            print("error direction")

    width = len(linout)
    if width > 0:
        p1 = ",".join(["FPGA_%s" % p for p in linout])
        p2 = ",".join(["fpga.%s" % p for p in linout])
        svia = """viabus #(.WIDTH(%d))
%svia (.a({%s}),
    .b({%s})
);"""
        sinout = svia % (width, name, p1, p2)

    return "\n".join(["\n".join(sinput), sinout, "\n".join(soutput)])


def inst(name, wirelist):
    return "\n,".join([".FPGA_%s(FPGA_%s)" % (p, p) for p in wirelist])


def xdc(name, pindict):
    s = """set_property -dict {PACKAGE_PIN %s IOSTANDARD %s} [get_ports {fpga_%s}]"""
    return "\n".join(
        [s % (p, pindict[p]["iostandard"], p.lower()) for p in pindict.keys() if "iostandard" in pindict[p]]
    )


# def xdc(name,wirelist):
#    s='''set_property -dict {PACKAGE_PIN %s IOSTANDARD LVCMOS18} [get_ports {FPGA_%s}]'''
#    return '\n'.join([s%(p,p.lower()) for p in wirelist])
# def pinslist(name,pins):
#    return '\n'.join(pins)


def masterdir(name, pindict):
    tcldir = {"input": "in", "output": "out", "inout": "inout"}
    s = []
    for p, d in pindict.items():
        s.append("%s %s" % (p, tcldir[d["dir"]]))
    return "\n".join(s)


if __name__ == "__main__":
    pins = [
        "G12",
        "G11",
        "B26",
        "E24",
        "G26",
        "J23",
        "L24",
        "P21",
        "AV21",
        "AR21",
        "C13",
        "D14",
        "D12",
        "D13",
        "AW18",
        "AV18",
        "BA19",
        "AP21",
        "AN14",
        "AP16",
        "AP14",
        "AU16",
        "AW12",
        "AY16",
        "BB12",
        "E25",
        "J14",
        "J13",
        "H13",
        "G13",
        "H15",
        "H14",
        "G16",
        "N16",
        "M16",
        "N15",
        "M15",
        "N14",
        "M14",
        "M17",
        "L17",
        "F13",
        "F14",
        "A14",
        "A15",
        "C16",
        "D16",
        "E15",
        "E16",
        "B15",
        "B16",
        "C14",
        "C15",
        "E14",
        "F15",
        "B12",
        "B13",
        "AP10",
        "AP11",
        "AR11",
        "AP12",
        "AT10",
        "AR10",
        "AT12",
        "AR12",
        "AU11",
        "AU12",
        "AV10",
        "AU10",
        "AW9",
        "AV9",
        "AW11",
        "AV11",
    ]
    import json
    import sys

    with open(sys.argv[1], "r") as f:
        pindict = json.load(f)
    pins = sorted(list(pindict.keys()))
    with open("fpga_port.vh", "w") as f:
        f.write(port("fpga", pindict))  # fpga_port.vh
    with open("fpga_if.sv", "w") as f:
        f.write(sv("fpga", pins))  # fpga_if.sv
    with open("fpga_via_sim.vh", "w") as f:
        f.write(via_sim("fpga", pindict))  # fpga_via_sim.vh
    with open("fpga_via.vh", "w") as f:
        f.write(via("fpga", pindict))  # fpga_via.vh
    with open("fpga_inst.vh", "w") as f:
        f.write(inst("fpga", pins))  # fpga_inst.vh
    with open("fpga.xdc", "w") as f:
        f.write(xdc("fpga", pindict))  # fpga.xdc
    #    with open("fpga_pins", 'w') as f:
    #        f.write(pinslist('fpga',pins))   #fpga_pins
    with open("fpga_master_dir", "w") as f:
        f.write(masterdir("fpga", pindict))  # fpga_pins
